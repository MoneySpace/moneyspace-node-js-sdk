import * as hjs from 'hash.js';

export function createTimeHash(): string {
    const d = new Date();
    return (
        `${d.getFullYear()}${pad(d.getMonth() + 1, 2)}${pad(d.getDate(), 2)}` +
        `${pad(d.getHours(), 2)}${pad(d.getMinutes(), 2)}${pad(d.getSeconds(), 2)}`
    );
}

export function pad(n: number, d: number): string {
    return String(n).padStart(d, '0');
}

export function hmacSHA256(key: string, data: string) {
    return hjs
        .hmac(hjs.sha256 as any, key)
        .update(data)
        .digest('hex');
}
