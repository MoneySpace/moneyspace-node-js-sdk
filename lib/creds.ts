import * as fs from 'fs';
import * as utl from 'util';
import { MoneySpaceCredentials, MoneySpaceCredentialsProvider } from './types';

/**
 * Get credentials provider which reads credentials from
 * JSON file with the following keys:
 *
 * - secretId
 * - secretKey
 *
 * @param jsonFile JSON credentials configuration
 */
export function createFileCredentialsProvider(jsonFile: string): MoneySpaceCredentialsProvider {
    return async (): Promise<MoneySpaceCredentials> => {
        const buf = await utl.promisify(fs.readFile)(jsonFile);
        const cfg = JSON.parse(buf.toString());
        if (typeof cfg['secretId'] != 'string') {
            return Promise.reject('Missing `secretId` configuration key');
        }
        if (typeof cfg['secretKey'] != 'string') {
            return Promise.reject('Missing `secretKey` configuration key');
        }
        return {
            secretId: cfg.secretId,
            secretKey: cfg.secretKey
        };
    };
}

/**
 * Get credentials provider configured by provided `secretId` and `secretKey` arguments.
 *
 * @param secretId
 * @param secretKey
 */
export function createSimpleCredentialsProvider(secretId: string, secretKey: string): MoneySpaceCredentialsProvider {
    return async (): Promise<MoneySpaceCredentials> => ({
        secretId,
        secretKey
    });
}
