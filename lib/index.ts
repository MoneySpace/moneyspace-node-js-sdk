export * from './creds';
import * as _got from 'got';
import { GotInstance, GotJSONFn } from 'got';
import * as http from 'http';
import * as qs from 'query-string';
import { StringDecoder } from 'string_decoder';
import {
    EventApiListener,
    EventApiType,
    EventPayment,
    MerchantPaymentRequest,
    MerchantProfile,
    MerchantTransactionId,
    MoneySpaceApi,
    MoneySpaceCredentials,
    MoneySpaceMerchantApi,
    MoneySpaceOptions,
    PaymentTransactionRef,
    PaymentTransactionStatus,
    WebHookRequestHandler
} from './types';
import { createTimeHash, hmacSHA256 } from './utils';

interface LocalGot extends GotInstance<GotJSONFn> {
}

// Listener: [event, listener, once, MerchantTransactionId]
type ListenerSlot = [EventApiType, EventApiListener, boolean, MerchantTransactionId?];

class MoneySpaceApiImpl implements MoneySpaceApi, MoneySpaceMerchantApi {
    constructor(opts: MoneySpaceOptions) {
        this.opts = {
            currency: 'THB',
            feeType: 'include',
            gatewayType: 'card',
            ...opts,
            ...(!opts.baseUrl ? {baseUrl: 'https://www.moneyspace.net/merchantapi'} : null)
        };
        this.got = _got.extend({
            baseUrl: this.opts.baseUrl,
            json: true,
            form: true
        });
    }

    readonly opts: Readonly<MoneySpaceOptions>;

    private readonly got: LocalGot;

    private listeners = Array<ListenerSlot>();

    // tslint:disable-next-line: variable-name
    private _credentials?: MoneySpaceCredentials;

    get merchantApi(): MoneySpaceMerchantApi {
        return this;
    }

    async sendPaymentTransaction(request: MerchantPaymentRequest): Promise<PaymentTransactionRef> {
        //
        const req = this.createEffectivePaymentRequest(request);
        const credentials = await this.credentials();
        const timeHash = createTimeHash();

        const hash = hmacSHA256(
            credentials.secretKey,
            [
                req.firstName,
                req.lastName,
                req.email || '',
                req.phone || '',
                req.amount,
                req.currency,
                req.productDescription || '',
                req.address || '',
                req.message || '',
                req.feeType,
                timeHash,
                req.customerOrderId,
                req.gatewayType,
                req.successUrl,
                req.failUrl,
                req.cancelUrl
            ].join('')
        );

        const resp = await this.got.post('v2/createpayment/obj', {
            body: {
                secreteID: credentials.secretId,
                firstname: req.firstName,
                lastname: req.lastName,
                amount: req.amount,
                currency: req.currency,
                feeType: req.feeType,
                timeHash,
                customer_order_id: req.customerOrderId,
                gatewayType: req.gatewayType,
                successUrl: req.successUrl,
                failUrl: req.failUrl,
                cancelUrl: req.cancelUrl,
                ...(req.message && {message: req.message}),
                ...(req.address && {address: req.address}),
                ...(req.phone && {phone: req.phone}),
                ...(req.productDescription && {description: req.productDescription}),
                ...(req.email && {email: req.email}),
                hash
            }
        });

        const payload = this.checkJSONResponse(resp);
        if (payload['Transaction ID'] == null) {
            throw new Error(payload.status); // seems to be an error
        }

        const ret = {
            orderId: req.customerOrderId,
            transactionId: payload['Transaction ID'],
            timeHash
        };

        this.notifyWithEvent({
            status: 'PaymentSent',
            transactionId: ret.transactionId,
            orderId: ret.orderId,
            amount: req.amount,
            timeHash
        });

        return ret;
    }

    async checkPaymentTransaction(request: PaymentTransactionRef): Promise<PaymentTransactionStatus> {
        const objRef = request.transactionId ? request.transactionId : request.orderId;
        if (objRef == null) {
            throw Error('`request` cannot be null');
        }
        const timeHash = createTimeHash();
        const credentials = await this.credentials();
        const resp = await this.got.post(
            objRef === request.transactionId ? 'v1/findbytransaction/obj' : 'v1/findbyorderid/obj',
            {
                body: {
                    secreteID: credentials.secretId,
                    ...(objRef === request.transactionId ? {transactionID: objRef} : {orderID: objRef}),
                    timeHash,
                    hash: hmacSHA256(credentials.secretKey, [objRef, timeHash].join(''))
                }
            }
        );

        let payload = this.checkJSONResponse2(resp);
        payload = Object.keys(payload).reduce(
            (pv, cv): { [key: string]: string } => {
                pv[cv.toLowerCase().trim()] = `${payload[cv]}`;
                return pv;
            },
            {} as { [key: string]: string }
        );

        return {
            amount: payload['amount'] || '',
            statusPayment: payload['status payment'] || '',
            description: payload['description'] || '',
            ...(payload['transaction id'] ? {transactionId: payload['transaction id']} : null),
            ...(payload['order id'] ? {orderId: payload['order id']} : null)
        };
    }

    async getPaymentLink(transactionId: MerchantTransactionId): Promise<string> {
        const timeHash = createTimeHash();
        const credentials = await this.credentials();
        const query = qs.stringify({
            secreteID: credentials.secretId,
            transactionID: transactionId,
            timehash: timeHash,
            hash: hmacSHA256(credentials.secretKey, [transactionId, timeHash].join(''))
        });
        return `${this.opts.baseUrl}/makepayment/linkpaymentcard?${query}`;
    }

    async getMerchantProfile(): Promise<MerchantProfile> {
        const timeHash = createTimeHash();
        const credentials = await this.credentials();
        const resp = await this.got.get('v1/store/obj', {
            query: qs.stringify({
                secreteID: credentials.secretId,
                timeHash,
                hash: hmacSHA256(credentials.secretKey, [timeHash, credentials.secretId].join(''))
            })
        });
        const payload = this.checkJSONResponse2(resp)['Store'] || [];
        return {
            stores: payload.map((s: any) => {
                s = s || {};
                return {
                    name: s.name || '',
                    logo: s.logo || '',
                    phone: s.telephone || ''
                };
            })
        };
    }

    registerApiEventListener(
        type: EventApiType | EventApiType[],
        listener: EventApiListener,
        once?: boolean,
        transactionId?: MerchantTransactionId
    ): any | any[] {
        if (Array.isArray(type)) {
            return type.map(t => this.registerApiEventListener(t, listener, once, transactionId));
        }
        this.listeners.push([type, listener, !!once, transactionId]);
        return listener;
    }

    unregisterApiEventListener(listenerReference: any | any[]): void {
        if (Array.isArray(listenerReference)) {
            listenerReference.map(r => this.unregisterApiEventListener(r));
        } else {
            this.listeners = this.listeners.filter(el => el[1] != listenerReference);
        }
    }

    private notifyWithEvent(event: EventPayment, transactionId?: MerchantTransactionId) {
        const rmlist = new Array<ListenerSlot>();
        try {
            this.listeners.forEach(l => {
                // Listener: [event, listener, once, tx]
                const [type, listener, once, tx] = l;
                if ((type == event.status || type == '*') && (tx == null || tx == transactionId)) {
                    // (type: MoneySpaceApiEventType | MoneySpaceApiEventType[], data: EventData): any;
                    listener(type, event);
                    if (once) {
                        rmlist.push(l);
                    }
                }
            });
        } finally {
            if (rmlist.length > 0) {
                this.listeners = this.listeners.filter(l => rmlist.indexOf(l) === -1);
            }
        }
    }

    private checkJSONResponse(resp: any): any {
        if (
            !Array.isArray(resp.body) ||
            resp.body[0] == null ||
            typeof resp.body[0] != 'object' ||
            typeof resp.body[0]['status'] != 'string'
        ) {
            throw new Error('Invalid API endpoint response');
        }
        return resp.body[0];
    }

    private checkJSONResponse2(resp: any): any {
        if (!Array.isArray(resp.body) || resp.body[0] == null || resp.body[0]['status'] != null) {
            const msg = (Array.isArray(resp.body) ? resp.body[0]['status'] : null) || 'Invalid API endpoint response';
            throw new Error(msg);
        }
        return resp.body[0];
    }

    private checkPaymentRequest(req: MerchantPaymentRequest & MoneySpaceOptions): typeof req {
        if (req.firstName == null) {
            throw new Error('Missing required `firstName` option');
        }
        if (req.lastName == null) {
            throw new Error('Missing required `lastName` option');
        }
        if (/^\d+\.\d{2}$/.exec(req.amount || '') == null) {
            throw new Error('Invalid format of `amount`');
        }
        if (['include', 'exclude'].indexOf(req.feeType || '') == -1) {
            throw new Error('Invalid `feeType` option');
        }
        if (req.customerOrderId == null) {
            throw new Error('Missing required `customerOrderId` option');
        }
        if (['card', 'qrnone'].indexOf(req.gatewayType || '') == -1) {
            throw new Error('Invalid `gatewayType` option');
        }
        if (req.gatewayType == 'card') {
            if (req.successUrl == null) {
                throw new Error('Missing required `successUrl` option');
            }
            if (req.failUrl == null) {
                throw new Error('Missing required `failUrl` option');
            }
            if (req.cancelUrl == null) {
                throw new Error('Missing required `cancelUrl` option');
            }
        }
        if (req.message && req.message.length > 100) {
            throw new Error('`message` option exceeded maximum allowed length of 100 characters');
        }
        if (req.gatewayType == 'qrnone' && req.productDescription == null) {
            throw new Error('For `qrnone` gateway require product description');
        }
        if (req.gatewayType == 'qrnone' && req.feeType != 'include') {
            throw new Error('For `qrnone` gateway feeType can be `include` only');
        }
        return req;
    }

    private async credentials(): Promise<MoneySpaceCredentials> {
        if (this._credentials) {
            return this._credentials;
        }
        const msc = this.opts.credentials;
        if (typeof msc == 'function') {
            this._credentials = await msc();
        } else {
            this._credentials = msc;
        }
        return this._credentials;
    }

    private createEffectivePaymentRequest(input: MerchantPaymentRequest): MerchantPaymentRequest & MoneySpaceOptions {
        const req = {...this.opts, ...input};
        this.checkPaymentRequest(req);
        return req;
    }

    createWebHookRequestHandler(): WebHookRequestHandler {
        return (req: http.IncomingMessage, resp: http.ServerResponse) => {
            resp.statusCode = 200;
            if (req.method != 'POST') {
                resp.statusCode = 400;
                resp.end();
                return;
            }
            const sd = new StringDecoder('utf8');
            const data = new Array<string>();
            req
                .on('data', chunk => data.push(sd.write(chunk)))
                .on('error', () => resp.end())
                .on('end', async () => {
                    data.push(sd.end());

                    const pq = qs.parse(data.join(''));
                    const params = Object.keys(pq).reduce(
                        (pv, cv): { [key: string]: string } => {
                            pv[cv.toLowerCase()] = `${pq[cv]}`;
                            return pv;
                        },
                        {} as { [key: string]: string }
                    );

                    const transactionId = params['transactionid'] != null ? params['transactionid'] : params['transectionid'];
                    const orderId = params['orderid'];
                    const amount = params['amount'];
                    const hash = params['hash'];
                    const status = params['status'];

                    if (status == null || transactionId == null || amount == null) {
                        resp.statusCode = 400;
                        resp.end();
                        return;
                    }
                    // Now process webHook data
                    const evt = {
                        transactionId,
                        orderId,
                        amount
                    } as EventPayment;

                    switch (status.toLowerCase()) {
                        case 'ok':
                            evt.status = 'PaymentReceived';
                            break;
                        case 'pending':
                            evt.status = 'PaymentPending';
                            break;
                        case 'pay success':
                            evt.status = 'PaymentSuccess';
                            break;
                        case 'fail':
                            evt.status = 'PaymentFailed';
                            break;
                        case 'cancel':
                            evt.status = 'PaymentCancelled';
                            break;
                    }
                    // Verify webHook request
                    const vbuf = [evt.transactionId, evt.amount];
                    const credentials = await this.credentials();
                    if (evt.status != 'PaymentReceived') {
                        vbuf.push(status);
                        vbuf.push(evt.orderId);
                    }
                    const vhash = hmacSHA256(credentials.secretKey, vbuf.join(''));
                    if (vhash != hash) {
                        resp.statusCode = 400;
                        resp.end();
                        return;
                    }

                    resp.end(); // Release conn before calling listeners
                    this.notifyWithEvent(evt, evt.transactionId);
                });
        };
    }
}

export function createMoneySpace(opts: MoneySpaceOptions): MoneySpaceApi {
    return new MoneySpaceApiImpl(opts);
}
