import * as http from 'http';

/**
 * Create MoneySpace API instance
 */
export function createMoneySpace(opts: MoneySpaceOptions): MoneySpaceApi;

/**
 * Get credentials provider which reads credentials from
 * JSON file with the following keys:
 *
 * - secretId
 * - secretKey
 *
 * @param jsonFile JSON credentials configuration
 */
export function createFileCredentialsProvider(jsonFile: string): MoneySpaceCredentialsProvider;

/**
 * Get credentials provider configured by provided `secretId` and `secretKey` arguments.
 *
 * @param secretId
 * @param secretKey
 */
export function createSimpleCredentialsProvider(secretId: string, secretKey: string): MoneySpaceCredentialsProvider;

/**
 * Available Merchant API event types
 */
type EventApiType =
    | '*'
    | 'PaymentSent'
    | 'PaymentReceived'
    | 'PaymentPending'
    | 'PaymentSuccess'
    | 'PaymentFailed'
    | 'PaymentCancelled';

type MerchantTransactionId = string;

type MerchantTransactionCurrency = 'THB'; // Only THB allowed

/**
 *  Payment gateway type.
 *
 * - Payment by card
 * - QR code payment
 */
type EventApi = EventPayment;

type EventPayment =
    | EventPaymentSent
    | EventPaymentReceived
    | EventPaymentPending
    | EventPaymentSuccess
    | EventPaymentFailed
    | EventPaymentCancelled;

/**
 * Merchant gateway type
 */
type MerchantPaymentGatewayType = 'card' | 'qrnone';

/**
 * - Fee on behalf of merchant: `include`
 * - Fee on behalf of shopper: `exclude`
 */
type MerchantTransactionFeeType = 'include' | 'exclude';

type MoneySpaceCredentialsProvider = () => Promise<MoneySpaceCredentials>;

type WebHookRequestHandler = (req: http.IncomingMessage, resp: http.ServerResponse) => any;

interface MerchantStore {
    name: string;
    logo?: string;
    phone?: string;
}

interface MerchantProfile {
    stores: MerchantStore[];
}

interface BasePaymentEvent {
    transactionId: MerchantTransactionId;
    orderId: string;
    amount: string;
    timeHash?: string;
}

interface EventPaymentSent extends BasePaymentEvent {
    status: 'PaymentSent';
}

interface EventPaymentReceived extends BasePaymentEvent {
    status: 'PaymentReceived';
}

interface EventPaymentPending extends BasePaymentEvent {
    status: 'PaymentPending';
}

interface EventPaymentSuccess extends BasePaymentEvent {
    status: 'PaymentSuccess';
}

interface EventPaymentCancelled extends BasePaymentEvent {
    status: 'PaymentFailed';
}

interface EventPaymentFailed extends BasePaymentEvent {
    status: 'PaymentCancelled';
}

interface PaymentTransactionStatus {
    /**
     * Order amount.
     * It must be string with two decimal places after point
     * and no comma in number.
     */
    amount: string;

    /**
     * Transaction status text
     */
    statusPayment: string;

    /**
     * Referenced transaction ID
     */
    transactionId?: MerchantTransactionId;

    /**
     * Merchant order ID.
     */
    orderId?: string;

    description?: string;
}

interface PaymentTransactionRef {
    transactionId?: string;
    orderId?: string;
    timeHash?: string;
}

type EventApiListener = (type: EventApiType | EventApiType[], data: EventApi) => any;

/**
 * MoneySpace access credentials.
 */
interface MoneySpaceCredentials {
    /**
     * Api access secret id
     */
    readonly secretId: string;

    /**
     * Api access secret key
     */
    readonly secretKey: string;
}

/**
 * MoneySpace access point options
 */
interface MoneySpaceOptions
    extends Partial<Pick<MerchantPaymentRequest, 'currency' | 'feeType' | 'gatewayType'>>,
        Partial<Pick<MerchantPaymentRequest, 'successUrl' | 'failUrl' | 'cancelUrl'>> {
    /**
     * Access credentials / credentials provider
     */
    credentials: MoneySpaceCredentials | MoneySpaceCredentialsProvider;

    /**
     * Default: https://www.moneyspace.net/merchantapi
     */
    baseUrl?: string;
}

/**
 * Payment request specification
 */
interface MerchantPaymentRequest {
    /**
     * Order amount.
     * It must be string with two decimal places after point
     * and no comma in number.
     */
    amount: string;

    /**
     * Internal merchant order id.
     * Order id must be unique over all merchant orders.
     * Length can't be over 20 symbols.
     */
    customerOrderId: string;

    /**
     * Shopper first name.
     */
    firstName: string;

    /**
     * Shopper last name.
     */
    lastName: string;

    /**
     * Note to the seller. Up to 100 characters.
     */
    message?: string;

    /**
     * Shopper email.
     */
    email?: string;

    /**
     * Shopper phone.
     */
    phone?: string;

    /**
     * Shopper's shipping address.
     */
    address?: string;

    /**
     * Transaction currency. Default THB.
     */
    currency?: MerchantTransactionCurrency;

    /**
     * Product/service details description. Required for gatewayType==`qrnone`
     */
    productDescription?: string;

    /**
     * Merchant transaction fee type. Default `exclude`
     */
    feeType?: MerchantTransactionFeeType;

    /**
     * Merchant gateway type. Default: `card`
     */
    gatewayType?: MerchantPaymentGatewayType;

    /**
     * As payment success, it will
     * redirect to the specific url.
     * Example: https://www.moneyspace.net/merchantapi/paycardsuccess
     * Required for gatewayType==`card`
     */
    successUrl?: string;

    /**
     * As payment failed, it will
     * redirect to the specific url.
     * Example: https://www.moneyspace.net?status=fail
     * Required for gatewayType==`card`
     */
    failUrl?: string;

    /**
     * As payment canceled, it will
     * redirect to the specific url.
     * Example: https://www.moneyspace.net?status=cancel
     * Required for gatewayType==`card`
     */
    cancelUrl?: string;
}

/**
 * MoneySpace Merchant API.
 */
interface MoneySpaceMerchantApi {
    /**
     * Sends payment transaction to MoneySpace endpoint.
     *
     * In the case of error promise will be resolved to `Error`
     * object describing an error state.
     *
     * @param request Transaction request object
     */
    sendPaymentTransaction(request: MerchantPaymentRequest): Promise<PaymentTransactionRef>;

    /**
     * Check status of payment transaction.
     * You may manually construct transaction `PaymentTransactionRef` object
     * providing either `transactionId` or `orderId`
     *
     * @param request Payment transaction reference.
     */
    checkPaymentTransaction(request: PaymentTransactionRef): Promise<PaymentTransactionStatus>;

    /**
     * Get payment link for transaction.
     */
    getPaymentLink(transactionId: MerchantTransactionId): Promise<string>;

    /**
     * Get merchant profile info
     */
    getMerchantProfile(): Promise<MerchantProfile>;
}

/**
 * MoneySpace SDK access point.
 */
interface MoneySpaceApi {
    /**
     * Merchant API access object
     */
    readonly merchantApi: MoneySpaceMerchantApi;

    /**
     * Registers an MoneySpace API events listener.
     *
     * @param type Listener will be subscribed to the specified of {@link EventApiType type}.
     *                  If `type` is `*` listener will be fired on all API events.
     * @param listener  Listener function.
     * @param [once] If `true` will run listener only once. Optional parameter, default: `false`.
     * @param transactionId Filter events only for specified transaction Id. Optional parameter, default: `null`.
     * @returns Listener registration object used to refer listener in {@link MoneySpaceApi#unregisterApiEventListener}
     */
    registerApiEventListener(
        type: EventApiType | EventApiType[],
        listener: EventApiListener,
        once?: boolean,
        transactionId?: MerchantTransactionId
    ): any | any[];

    /**
     * Unregister AOI event listener.
     * @param listenerReference Listener references got from {@link MoneySpaceApi#registerApiEventListener}
     */
    unregisterApiEventListener(listenerReference: any | any[]): void;

    /**
     * WebHook
     */
    createWebHookRequestHandler(): WebHookRequestHandler;
}
