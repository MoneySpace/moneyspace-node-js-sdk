const path = require('path');
const os = require('os');
const express = require('express');
const bodyParser = require('body-parser');
const uuid = require('uuid');
const ms = require('@moneyspace.net/moneyspace-node-js-sdk');

const api = ms.createMoneySpace({
    credentials: ms.createFileCredentialsProvider(path.join(os.homedir(), '.moneyspace-test-creds.json'))
});

const app = express();
app.use(bodyParser.urlencoded());
app.use(express.static('public'));
app.post('/create', async function (req, res, next) {
    try {
        const transactionResponse = await api.merchantApi.sendPaymentTransaction({
            amount: '0.25',
            customerOrderId: uuid.v4().substring(0, 10),
            firstName: 'John',
            lastName: 'Doe',
            gatewayType: 'qrnone',
            productDescription: 'Ice cream' // required for qrnone
        });
        console.log('Payment transaction:', transactionResponse);
        const link = await api.merchantApi.getPaymentLink(transactionResponse.transactionId);
        return res.send({transactionId: transactionResponse.transactionId, link});
    } catch (e) {
        console.log(e);
        return res.status(500).send('Internal server error');
    }
});

app.post('/check', async function (req, res, next) {
    let transactionId = !!req.body.transactionId ? req.body.transactionId.trim() : null;
    if (!transactionId) {
        return res.status(400).send('transactionId is required');
    }
    try {
        const details = await api.merchantApi.checkPaymentTransaction({transactionId});
        console.log(details);
        return res.send(details);
    } catch (e) {
        console.log(e);
        return res.status(500).send('Internal server error');
    }
});

app.listen(8080, function () {
    console.log('HTTP server running: http://localhost:8080');
});
