### Before run

Create json credentials configuration file: `${HOME}/.moneyspace-test-creds.json` with the following
content:

```json
{
  "secretId": "...",
  "secretKey": "..."
}
```

### Run qrnone example

```sh
yarn install
yarn run build
node ./template-qrnone.js
```
