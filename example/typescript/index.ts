import * as http from 'http';
import {
    createMoneySpace,
    createSimpleCredentialsProvider,
    EventApi,
    EventApiType,
    MerchantPaymentRequest,
    MoneySpaceApi,
    PaymentTransactionRef
} from '@moneyspace.net/moneyspace-node-js-sdk';

/// Create payment with no WebHooks
async function createPayment(api?: MoneySpaceApi,
                             paymentRequest?: MerchantPaymentRequest): Promise<PaymentTransactionRef> {
    // Get MoneySpace SDK API endpoint using minimal allowed configuration:
    api = api || createMoneySpace({
        credentials: createSimpleCredentialsProvider(
            '<secret id>',
            '<secret key>'
        ),
        successUrl: 'https://www.moneyspace.net/merchantapi/paycardsuccess',
        cancelUrl: 'https://www.moneyspace.net?status=cancel',
        failUrl: 'https://www.moneyspace.net?status=fail'
    });

    // Now send payment request
    const merchantApi = api.merchantApi;

    // Initiate payment transaction using minimal allowed configuration:
    const response = await merchantApi.sendPaymentTransaction(paymentRequest || {
        amount: '<amount>',
        customerOrderId: '<unique customer order id>',
        firstName: '<customer first name>',
        lastName: '<customer last name>'
    });

    console.log('Payment transaction: ', response);

    // Now we got JSON response like this:
    //
    //    transactionId: 'MSTRF18000000017437',
    //    orderId: '<unique customer order id>',
    //    timeHash: '<transaction timestamp>'
    return response;
}

async function createPaymentWithWebHooks() {
    const api = createMoneySpace({
        credentials: createSimpleCredentialsProvider('<secret id>', '<secret key>'),
        successUrl: 'https://www.moneyspace.net/merchantapi/paycardsuccess',
        cancelUrl: 'https://www.moneyspace.net?status=cancel',
        failUrl: 'https://www.moneyspace.net?status=fail'
    });

    // First of all we have to start WebHook webserver.
    // You can use pure NodeJS HTTP server or any framework you like
    http
        .createServer(api.createWebHookRequestHandler())
        .on('error', function (err) {
            console.log(err);
        })
        .listen({
            host: 'localhost',
            port: 8080
        }, () => console.log('HTTP WebHook server running'));

    // Register listener for all WebHook events
    api.registerApiEventListener('*', (type: EventApiType | EventApiType[], event: EventApi) => {
        console.log('Got WebHook event: ', event);
        if (type == 'PaymentSuccess') {
            console.log(`Order: ${event.orderId} paid successfully`);
        }
    });

    // Then send the actual transaction
    await createPayment(api);
}

async function createQRPayment() {
    const api = createMoneySpace({
        credentials: createSimpleCredentialsProvider('<secret id>', '<secret key>'),
    });

    // Then send the actual transaction
    const transactionResponse = await createPayment(api, {
        amount: '<amount>',
        customerOrderId: '<unique customer order id>',
        firstName: '<customer first name>',
        lastName: '<customer last name>',
        gatewayType: 'qrnone',
        productDescription: '<payment description>'
    });

    const link = await api.merchantApi.getPaymentLink(transactionResponse.transactionId!);

    // link contain QR Code for payment. Show it to user.
    console.log('link', link);

    let timer: any;
    const checkPayment = async function () {
        const details = await api.merchantApi.checkPaymentTransaction({
            transactionId: transactionResponse.transactionId!
        });
        console.log(details);
        switch (details.statusPayment.toLowerCase()) {
            case 'ok':
            case 'pending':
                // wait more
                timer = setTimeout(checkPayment, 5000);
                break;
            case 'pay success':
                // hide QR code and show success page to user
                console.log('Payment complete');
                break;
            case 'fail':
                // hide QR code and show fail page to user
                console.log('Payment failed');
                break;
            case 'cancel':
                // hide QR code and show fail page to user
                console.log('Payment canceled');
                break;
            default:
                console.log('Unknown response!');
                break;
        }
    };
    timer = setTimeout(checkPayment, 5000);
}
