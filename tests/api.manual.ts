import * as http from 'http';
import * as os from 'os';
import * as path from 'path';
import * as uuid from 'uuid';
import { createFileCredentialsProvider, createMoneySpace } from '../lib';

async function run() {
    const baseUrl = process.env['HOST'] || undefined;
    console.log(`Base url: ${baseUrl}`);
    const api = createMoneySpace({
        baseUrl,
        credentials: createFileCredentialsProvider(path.join(os.homedir(), '.moneyspace-test-creds.json')),
        successUrl: 'https://www.moneyspace.net/merchantapi/paycardsuccess',
        cancelUrl: 'https://www.moneyspace.net?status=cancel',
        failUrl: 'https://www.moneyspace.net?status=fail'
    });

    const port = 8080;
    const webHookHandler = api.createWebHookRequestHandler();
    const server = http.createServer((req, resp) => {
        webHookHandler(req, resp);
    });
    server
        .on('error', function (e) {
            console.error(e);
        })
        .listen(
            {
                host: '0.0.0.0',
                port
            },
            () => {
                console.log(`HTTP Server running at ${port} on 0.0.0.0`);
            }
        );

    // const customerOrderId = uuid.v4().substring(0, 10);
    // const transactionResponse = await api.merchantApi.sendPaymentTransaction({
    //     amount: '0.25',
    //     customerOrderId,
    //     firstName: 'John',
    //     lastName: 'Doe',
    // });
    // console.log('transactionResponse', transactionResponse);
    // const link = await api.merchantApi.getPaymentLink(transactionResponse.transactionId!);
    // console.log('link', link);

    // const details = await api.merchantApi.checkPaymentTransaction({ transactionId: 'MSTRF18000000024765' });
    // const details = await api.merchantApi.checkPaymentTransaction({orderId: customerOrderId});
    // console.log('Details ', details);
}

run();
