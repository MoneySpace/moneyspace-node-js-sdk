import test from 'ava';
import * as hjs from 'hash.js';
import * as path from 'path';
import { createFileCredentialsProvider } from '../lib';
import { createTimeHash } from '../lib/utils';

test('time hash', t => {
    t.is(createTimeHash().length, 'yyyyMMddHHmmss'.length);
});

test('compute request hash', t => {
    const sink = 'MSTRF1800000001743720190805221708';
    const key = 'JJoa97DitZ8rJkWrmRTgg7573htuoi6iaY5ux92l8UhF0efP6a0xhgdS0Jd62676Wl0Qfn60gLGv36Pp';
    const digest = hjs
        .hmac(hjs.sha256 as any, key)
        .update(sink)
        .digest('hex');
    t.is(digest, 'a0e90f9039a789b6cf8040ca6fc0d3b0dac8644f51759a9b6ab2e111dc4719bb');
});

test('file api credentials provider', async t => {
    const provider = createFileCredentialsProvider(path.join(__dirname, 'file_creds.json'));
    const credentials = await provider();
    t.is(credentials.secretId, '281b059a0d2e4b57a837252d89de7b6b');
    t.is(credentials.secretKey, 'e75c28d544e24e14a5101996fa3be6b3');
});
