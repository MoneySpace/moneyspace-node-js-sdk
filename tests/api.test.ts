import test, { after, before } from 'ava';
import * as nock from 'nock';
import { createMoneySpace, createSimpleCredentialsProvider } from '../lib';
import * as qs from 'query-string';
import { MoneySpaceApi } from '../lib/types';

let nockScope: nock.Scope;

function createApi(): MoneySpaceApi {
    return createMoneySpace({
        credentials: createSimpleCredentialsProvider(
            'secret:e5e362183961480383d7a9662cbb3a5f',
            'key:2e5475b2ea40440b94176f4c138d21d6'
        ),
        successUrl: 'https://www.moneyspace.net/merchantapi/paycardsuccess',
        cancelUrl: 'https://www.moneyspace.net?status=cancel',
        failUrl: 'https://www.moneyspace.net?status=fail'
    });
}

before(() => {
    nockScope = nock('https://www.moneyspace.net');
    nock.disableNetConnect();
});

after(() => {
    nock.cleanAll();
    nock.enableNetConnect();
});

test('mocked payment transaction', async t => {
    nockScope.post('/merchantapi/v2/createpayment/obj').reply(200, () => {
        return [
            {
                status: 'Create Success',
                'Transaction ID': 'MSTRF18000000017437'
            }
        ];
    });

    const api = createApi();
    const resp = await api.merchantApi.sendPaymentTransaction({
        amount: '0.15',
        customerOrderId: 'f229c529d4c347449d6fb09c51b979a0',
        firstName: 'John',
        lastName: 'Doe'
    });
    t.is(resp.orderId, 'f229c529d4c347449d6fb09c51b979a0');
    t.is(resp.transactionId, 'MSTRF18000000017437');
    t.truthy(resp.timeHash);
});

test('mocked payment rejection', async t => {
    nockScope.post('/merchantapi/v2/createpayment/obj').reply(200, () => {
        return [
            {
                status: 'NotFound'
            }
        ];
    });

    const api = createApi();
    const err = await t.throwsAsync(
        api.merchantApi.sendPaymentTransaction({
            amount: '0.15',
            customerOrderId: 'f229c529d4c347449d6fb09c51b979a0',
            firstName: 'John',
            lastName: 'Doe'
        })
    );
    t.is(err.message, 'NotFound');
});

test('mocked check payment transaction', async t => {
    nockScope.post('/merchantapi/v1/findbytransaction/obj').reply(200, (uri, requestBody) => {
        const body = qs.parse(requestBody.toString());
        return [
            {
                Amount: '100.25',
                'transaction ID': body.transactionID,
                'Status Payment': 'pending',
                Description: 'Tx description'
            }
        ];
    });
    const api = createApi();
    const resp = await api.merchantApi.checkPaymentTransaction({
        transactionId: 'MSTRF18000000017437'
    });
    t.is(resp.transactionId, 'MSTRF18000000017437');
    t.is(resp.amount, '100.25');
});

test('mocked check payment transaction rejection', async t => {
    nockScope.post('/merchantapi/v1/findbytransaction/obj').reply(200, (uri, requestBody) => {
        return [
            {
                status: 'You cannot have permission to view this transaction.'
            }
        ];
    });
    const api = createApi();
    const err = await t.throwsAsync(
        api.merchantApi.checkPaymentTransaction({
            transactionId: 'MSTRF18000000017437'
        })
    );
    t.is(err.message, 'You cannot have permission to view this transaction.');
});

test('mocked check order id', async t => {
    nockScope.post('/merchantapi/v1/findbyorderid/obj').reply(200, (uri, requestBody) => {
        const body = qs.parse(requestBody.toString());
        return [
            {
                Amount: '100.25',
                'Order ID': body.orderID,
                'Status Payment': 'pending',
                Description: 'Tx description'
            }
        ];
    });
    const api = createApi();
    const resp = await api.merchantApi.checkPaymentTransaction({
        orderId: 'd8a3287c'
    });
    t.is(resp.orderId, 'd8a3287c');
    t.is(resp.amount, '100.25');
});

test('mocked check merchant profile', async t => {
    nockScope.get(/\/merchantapi\/v1\/store\/obj.*/).reply(200, () => {
        return [
            {
                Store: [
                    {
                        name: 'test store',
                        logo: 'https://moneyspace.s3-ap-southeast-1.amazonaws.com/1194_20190707220401917.jpeg',
                        telephone: '0999999903'
                    }
                ]
            }
        ];
    });
    const api = createApi();
    const resp = await api.merchantApi.getMerchantProfile();
    t.true(resp.stores && Array.isArray(resp.stores));
    const store = resp.stores[0] || {};
    t.is(store.name, 'test store');
    t.is(store.logo, 'https://moneyspace.s3-ap-southeast-1.amazonaws.com/1194_20190707220401917.jpeg');
    t.is(store.phone, '0999999903');
});


